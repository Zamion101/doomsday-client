import java.io.File;
import java.lang.reflect.Field;
import java.util.Arrays;

import net.minecraft.client.main.Main;
import org.lwjgl.LWJGLUtil;

public class Start
{
    public static void main(String[] args)
    {
        //System.setProperty("java.library.path",new File("C:\\Users\\berke\\Desktop\\DoomsDay\\jars\\versions\\1.8.8\\1.8.8-natives").getAbsolutePath());

        File JGLLib = new File("versions/1.8.8/1.8.8-natives");

        System.setProperty("org.lwjgl.librarypath", JGLLib.getAbsolutePath());


        Main.main(concat(new String[]{"--version", "mcp", "--accessToken", "0", "--assetsDir", "assets", "--assetIndex", "1.8", "--userProperties", "{}"}, args));
    }

    public static <T> T[] concat(T[] first, T[] second)
    {
        T[] result = Arrays.copyOf(first, first.length + second.length);
        System.arraycopy(second, 0, result, first.length, second.length);
        return result;
    }
}
