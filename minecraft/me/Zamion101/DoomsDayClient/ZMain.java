package me.Zamion101.DoomsDayClient;

import me.Zamion101.DoomsDayClient.commands.*;
import me.Zamion101.DoomsDayClient.utils.CommandManager;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.network.*;
import net.minecraft.network.play.client.C00PacketKeepAlive;
import net.minecraft.network.play.client.C03PacketPlayer;

import java.util.ArrayList;
import java.util.List;


public class ZMain {

    static ZMain theZMain;

    EntityPlayerMP playerMP;
    NetHandlerPlayServer NHPS;
    NetworkManager NM;
    List<Class<? extends Packet>> bannedPackets = new ArrayList<>();

    public ZMain(){
        theZMain = this;
        initCommands();
        initPackets();
    }

    void initPackets(){
        bannedPackets.add(C03PacketPlayer.class);
        bannedPackets.add(C03PacketPlayer.C06PacketPlayerPosLook.class);
        bannedPackets.add(C03PacketPlayer.C05PacketPlayerLook.class);
        bannedPackets.add(C03PacketPlayer.C04PacketPlayerPosition.class);
        bannedPackets.add(C00PacketKeepAlive.class);
        bannedPackets.add(C03PacketPlayer.class);
    }


    public void initCommands(){
        CommandManager.addCommand(new DCommands("info",new InfoCommand()));
        CommandManager.addCommand(new DCommands("teleport",new TeleportCommand()));
        CommandManager.addCommand(new DCommands("tp",new TeleportCommand()));
        CommandManager.addCommand(new DCommands("fly",new FlyCommand()));
        CommandManager.addCommand(new DCommands("give",new GiveCommand()));
    }


    public boolean readPacket(Packet packet, NetworkManager manager){
        NM = manager;
        if(EnumConnectionState.getPacketDirection(packet) != null && EnumConnectionState.getPacketDirection(packet).equals(EnumPacketDirection.CLIENTBOUND)){
            System.out.println("[CB]" + packet);
        }
        if(EnumConnectionState.getPacketDirection(packet) != null && EnumConnectionState.getPacketDirection(packet).equals(EnumPacketDirection.SERVERBOUND)){
            if(!bannedPackets.contains(packet.getClass())){
                System.out.println("[SB]" + packet);
            }
        }
        return true;
    }

    public void setNHPS(NetHandlerPlayServer nhps){
        NHPS = nhps;
    }

    public void setPlayerMP(EntityPlayerMP player){
        playerMP = player;
    }

    public static ZMain getZMain() {
        return theZMain;
    }

    public EntityPlayerMP getPlayerMP() {
        return playerMP;
    }

    public NetHandlerPlayServer getNHPS() {
        return NHPS;
    }

    public NetworkManager getNM() {
        return NM;
    }
}
