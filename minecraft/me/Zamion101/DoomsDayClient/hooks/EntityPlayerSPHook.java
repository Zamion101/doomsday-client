package me.Zamion101.DoomsDayClient.hooks;

import me.Zamion101.DoomsDayClient.utils.CommandManager;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.client.network.NetHandlerPlayClient;
import net.minecraft.network.play.client.C01PacketChatMessage;
import net.minecraft.network.play.client.C16PacketClientStatus;
import net.minecraft.stats.StatFileWriter;
import net.minecraft.world.World;

public class EntityPlayerSPHook extends EntityPlayerSP
{
    public EntityPlayerSPHook(Minecraft mcIn, World worldIn, NetHandlerPlayClient p_i46278_3_, StatFileWriter p_i46278_4_)
    {
        super(mcIn, worldIn, (NetHandlerPlayClientHook)p_i46278_3_, p_i46278_4_);
    }

    public void respawnPlayer()
    {
        this.sendQueue.addToSendQueue(new C16PacketClientStatus(C16PacketClientStatus.EnumState.PERFORM_RESPAWN));
    }
}