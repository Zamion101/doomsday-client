package me.Zamion101.DoomsDayClient.commands;

import me.Zamion101.DoomsDayClient.utils.Inventory;
import me.Zamion101.DoomsDayClient.utils.Wrapper;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.network.play.server.S39PacketPlayerAbilities;

public class GiveCommand implements ICommand {


    @Override
    public void execute(String[] args, EntityPlayerSP player) {
        try {
            ItemStack itemstack = new ItemStack(Items.diamond,1,0);
            Wrapper.getPlayer().inventory.addItemStackToInventory(itemstack);
            Inventory.updateInventory();
        }catch (Exception e){
            e.printStackTrace();
        }
    }


}
