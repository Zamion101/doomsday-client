package me.Zamion101.DoomsDayClient.commands;

import me.Zamion101.DoomsDayClient.ZMain;
import me.Zamion101.DoomsDayClient.utils.Wrapper;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.network.play.client.C03PacketPlayer;

public class TeleportCommand implements ICommand {

    @Override
    public void execute(String[] args, EntityPlayerSP player) {
        float $posZ;
        if (args.length == 3) {
            try {
                float posX = args[0].contains("~") ?
                        (float) Wrapper.getPlayer().posX + (
                                args[0].length() == 1 ? 0.0F : Float.parseFloat(args[0].substring(1))) :
                        Float.parseFloat(args[0]);
                float posY = args[1].contains("~") ?
                        (float) Wrapper.getPlayer().posY + (
                                args[1].length() == 1 ? 0.0F : Float.parseFloat(args[1].substring(1))) :
                        Float.parseFloat(args[1]);
                $posZ = args[2].contains("~") ?
                        (float) Wrapper.getPlayer().posZ + (
                                args[2].length() == 1 ? 0.0F : Float.parseFloat(args[2].substring(1))) :
                        Float.parseFloat(args[2]);

                Wrapper.getPlayer().setLocationAndAngles(posX, posY, $posZ, Wrapper.getPlayer().rotationYaw,
                        Wrapper.getPlayer().rotationPitch);
                Wrapper.getSendQueue()
                        .addToSendQueue(new C03PacketPlayer.C04PacketPlayerPosition(posX, posY, $posZ, true));

                System.out.println("Teleported to: " + (int) Wrapper.getPlayer().posX + " " +
                        (int) Wrapper.getPlayer().posY + " " + (int) Wrapper.getPlayer().posZ + ".");
            } catch (NumberFormatException e) {
                System.out.println("Couldn't parse an integer from: " + e.getCause() + ".");
            }
        } else if (args.length == 1) {
            boolean success = false;
            for (EntityPlayer e : Wrapper.getWorld().playerEntities) {
                if (e.getName().equalsIgnoreCase(args[0])) {
                    success = true;
                    int posX = (int) e.posX;
                    int posY = (int) e.posY;
                    int posZ = (int) e.posZ;

                    Wrapper.getPlayer().setLocationAndAngles(posX, posY, posZ, Wrapper.getPlayer().rotationYaw,
                            Wrapper.getPlayer().rotationPitch);
                    ZMain.getZMain().getNM().sendPacket(new C03PacketPlayer.C04PacketPlayerPosition(posX, posY, posZ, true));
                    Wrapper.getSendQueue()
                            .addToSendQueue(new C03PacketPlayer.C04PacketPlayerPosition(posX, posY, posZ, true));

                    System.out.println("Teleported to: " + (int) Wrapper.getPlayer().posX + " " +
                            (int) Wrapper.getPlayer().posY + " " + (int) Wrapper.getPlayer().posZ + ".");
                    break;
                }
            }
            if (!success) {
                System.out.println("Could not find player by the name of: " + args[0] + ".");
            }
        } else {
            System.out.println("Please provide the X, Y, and Z coordinates, or a player's name.");
        }
    }
}
