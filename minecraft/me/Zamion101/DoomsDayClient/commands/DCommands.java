package me.Zamion101.DoomsDayClient.commands;

public class DCommands {


    String command;
    ICommand module;

    public DCommands(String command, ICommand module) {
        this.command = command;
        this.module = module;
    }

    public String getCommand() {
        return command;
    }

    public ICommand getModule() {
        return module;
    }
}
