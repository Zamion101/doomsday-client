package me.Zamion101.DoomsDayClient.commands;

import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.entity.player.EntityPlayerMP;

import java.util.Arrays;

public class InfoCommand implements ICommand {

    @Override
    public void execute(String[] args, EntityPlayerSP player) {
        Minecraft mc = Minecraft.getMinecraft();

        System.out.println(player);
        System.out.println(mc.theWorld.isRemote);
        System.out.println(mc.theWorld);
        System.out.println(mc.theWorld.getPlayerEntityByUUID(mc.thePlayer.getUniqueID()));
        System.out.println(mc.theWorld.loadedEntityList);
        System.out.println(Arrays.toString(mc.theWorld.playerEntities.toArray()));
        System.out.println(mc.currentScreen);
        System.out.println(mc.thePlayer.onGround);
        System.out.println(mc.thePlayer.isServerWorld());
        try{
            EntityPlayerMP mp = (EntityPlayerMP) mc.theWorld.playerEntities.get(0);
            System.out.println(mp.getName());
        }catch (Exception e){
            System.out.println("No you can't!");
        }
    }
}
