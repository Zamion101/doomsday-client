package me.Zamion101.DoomsDayClient.commands;

import net.minecraft.client.entity.EntityPlayerSP;

public interface ICommand {


    void execute(String[] args, EntityPlayerSP player);
}
