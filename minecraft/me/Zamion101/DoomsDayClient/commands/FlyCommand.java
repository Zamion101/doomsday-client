package me.Zamion101.DoomsDayClient.commands;

import io.netty.buffer.ByteBuf;
import me.Zamion101.DoomsDayClient.ZMain;
import me.Zamion101.DoomsDayClient.utils.Wrapper;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.network.PacketBuffer;
import net.minecraft.network.play.client.C13PacketPlayerAbilities;
import net.minecraft.network.play.server.S39PacketPlayerAbilities;

public class FlyCommand implements ICommand{


    Thread t;
    public static boolean fly = false;
    @Override
    public void execute(String[] args, EntityPlayerSP player) {
        fly = true;
        if(args.length == 0){
            System.out.println("ARGS BOŞ!");
            return;
        }
        if(args[0].equalsIgnoreCase("on")){
            t = new Thread(() -> {
                while (fly){
                    Wrapper.getPlayer().capabilities.allowFlying = true;
                    Wrapper.getPlayer().capabilities.isFlying = true;
                }
            });
            C13PacketPlayerAbilities ppa = new C13PacketPlayerAbilities(Wrapper.getPlayer().capabilities);
            ZMain.getZMain().getNM().sendPacket(ppa);
            System.out.println("Allowed!");

            t.start();
        }else if(args[0].equalsIgnoreCase("off")){
            Wrapper.getPlayer().capabilities.allowFlying = true;
            Wrapper.getPlayer().capabilities.isFlying = false;
            fly = false;
            t.stop();
        }else{
            System.out.println("HATA! FLY");
            return;
        }
    }
}
