package me.Zamion101.DoomsDayClient.utils;

import com.sun.org.apache.xpath.internal.operations.Mod;
import me.Zamion101.DoomsDayClient.hooks.EntityPlayerSPHook;
import me.Zamion101.DoomsDayClient.hooks.NetHandlerPlayClientHook;
import net.minecraft.block.Block;
import net.minecraft.block.BlockAir;
import net.minecraft.block.BlockLiquid;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.multiplayer.PlayerControllerMP;
import net.minecraft.client.multiplayer.WorldClient;
import net.minecraft.client.network.NetHandlerPlayClient;
import net.minecraft.client.settings.GameSettings;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.network.Packet;
import net.minecraft.network.play.client.C01PacketChatMessage;
import net.minecraft.network.play.client.C03PacketPlayer;
import net.minecraft.util.*;

import java.util.List;

public class Wrapper {

    public static boolean isMoving(Entity e)
    {
        return (e.motionX != 0.0D) && (e.motionZ != 0.0D) && ((e.motionY != 0.0D) || (e.motionY > 0.0D));
    }

    public static Block getBlock(BlockPos pos)
    {
        return getMinecraft().theWorld.getBlockState(pos).getBlock();
    }

    public static Block getBlockAbovePlayer(EntityPlayer inPlayer, double blocks)
    {
        blocks += inPlayer.height;
        return getBlock(new BlockPos(inPlayer.posX, inPlayer.posY + blocks, inPlayer.posZ));
    }

    public static Block getBlockAtPos(EntityPlayer inPlayer, double x, double y, double z)
    {
        return getBlock(new BlockPos(inPlayer.posX - x, inPlayer.posY - y, inPlayer.posZ - z));
    }

    public static boolean isOnLiquid()
    {
        boolean onLiquid = false;
        if ((getBlockAtPos(getMinecraft().thePlayer, 0.30000001192092896D, 0.10000000149011612D, 0.30000001192092896D).getMaterial().isLiquid()) && (getBlockAtPos(getMinecraft().thePlayer, -0.30000001192092896D, 0.10000000149011612D, -0.30000001192092896D).getMaterial().isLiquid())) {
            onLiquid = true;
        }
        return onLiquid;
    }

    public static Block getBlockUnderPlayer(EntityPlayer inPlayer)
    {
        return getBlock(new BlockPos(inPlayer.posX, inPlayer.posY - 1.0D, inPlayer.posZ));
    }

    public static Minecraft mc = Minecraft.getMinecraft();

    public static double getDistance(Entity e, BlockPos pos)
    {
        return MathHelper.sqrt_float((float)(e.posX - pos.getX()) * (float)(e.posX - pos.getX()) +
                (float)(e.posY - pos.getY()) * (float)(e.posY - pos.getY()) +
                (float)(e.posZ - pos.getZ()) * (float)(e.posZ - pos.getZ()));
    }

    public static float getDistanceToGround(Entity e)
    {
        if (getPlayer().isCollidedVertically) {
            return 0.0F;
        }
        for (float a = (float)e.posY; a > 0.0F; a -= 1.0F)
        {
            int[] stairs = { 53, 67, 108, 109, 114, 128, 134, 135, 136, 156, 163, 164, 180 };
            int[] exemptIds = { 6, 27, 28, 30, 31, 32, 37, 38, 39, 40, 50, 51, 55, 59, 63, 65, 66, 68, 69, 70, 72, 75,
                    76, 77, 83, 92, 93, 94, 104, 105, 106, 115, 119, 131, 132, 143, 147, 148, 149, 150, 157, 171, 175,
                    176, 177 };

            Block block = getBlock(new BlockPos(e.posX, a - 1.0F, e.posZ));
            if (!(block instanceof BlockAir))
            {
                if ((Block.getIdFromBlock(block) == 44) ||
                        (Block.getIdFromBlock(block) == 126)) {
                    return (float)(e.posY - a - 0.5D) < 0.0F ? 0.0F : (float)(e.posY - a - 0.5D);
                }
                int[] arrayOfInt1;
                int j = (arrayOfInt1 = stairs).length;
                for (int i = 0; i < j; i++)
                {
                    int id = arrayOfInt1[i];
                    if (Block.getIdFromBlock(block) == id) {
                        return (float)(e.posY - a - 1.0D) < 0.0F ? 0.0F : (float)(e.posY - a - 1.0D);
                    }
                }
                j = (arrayOfInt1 = exemptIds).length;
                for (int i = 0; i < j; i++)
                {
                    int id = arrayOfInt1[i];
                    if (Block.getIdFromBlock(block) == id) {
                        return (float)(e.posY - a) < 0.0F ? 0.0F : (float)(e.posY - a);
                    }
                }
                return (float)(e.posY - a + block.getBlockBoundsMaxY() - 1.0D);
            }
        }
        return 0.0F;
    }

    public static FontRenderer getFontRenderer()
    {
        return getMinecraft().fontRendererObj;
    }

    public static GameSettings getGameSettings()
    {
        return getMinecraft().gameSettings;
    }

    public static InventoryPlayer getInventory()
    {
        return getPlayer().inventory;
    }

    public static Minecraft getMinecraft()
    {
        return Minecraft.getMinecraft(

        );
    }

    public static MovingObjectPosition getMouseOver()
    {
        return getMinecraft().objectMouseOver;
    }

    public static EntityPlayerSP getPlayer() {
        return getMinecraft().thePlayer;
    }

    public static PlayerControllerMP getPlayerController()
    {
        return getMinecraft().playerController;
    }

    public static NetHandlerPlayClient getSendQueue()
    {
        return getPlayer().sendQueue;
    }

    public static Session getSession()
    {
        return getMinecraft().session;
    }

    public static WorldClient getWorld()
    {
        return getMinecraft().theWorld;
    }

    public static FontRenderer fr = Minecraft.getMinecraft().fontRendererObj;

    public static boolean isInBlock(Entity e, float offset)
    {
        for (int x = MathHelper.floor_double(e.getEntityBoundingBox().minX); x <
                MathHelper.floor_double(e.getEntityBoundingBox().maxX) + 1; x++) {
            for (int y = MathHelper.floor_double(e.getEntityBoundingBox().minY); y <
                    MathHelper.floor_double(e.getEntityBoundingBox().maxY) + 1; y++) {
                for (int z = MathHelper.floor_double(e.getEntityBoundingBox().minZ); z <
                        MathHelper.floor_double(e.getEntityBoundingBox().maxZ) + 1; z++)
                {
                    Block block = getWorld().getBlockState(new BlockPos(x, y + offset, z)).getBlock();
                    if ((block != null) && (!(block instanceof BlockAir)) && (!(block instanceof BlockLiquid)))
                    {
                        AxisAlignedBB boundingBox = block.getCollisionBoundingBox(getWorld(),
                                new BlockPos(x, y + offset, z),
                                getWorld().getBlockState(new BlockPos(x, y + offset, z)));
                        if ((boundingBox != null) && (e.getEntityBoundingBox().intersectsWith(boundingBox))) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    public static void sendChatMessage(String msg)
    {
        sendPacket(new C01PacketChatMessage(msg));
    }

    public static void sendPacket(Packet packet)
    {
        getSendQueue().addToSendQueue(packet);
    }

    public static void sendPacketDirect(Packet packet)
    {
        NetHandlerPlayClientHook.netManager.sendPacket(packet);
    }

    public Float[] getRotationToPosition(Entity e, BlockPos pos)
    {
        double x = pos.getX() - e.posX;double y = pos.getY() - (e.posY + e.getEyeHeight());double z = pos.getZ() - e.posZ;
        double helper = MathHelper.sqrt_double(x * x + z * z);
        float newYaw = (float)Math.toDegrees(-Math.atan(x / z));
        float newPitch = (float)-Math.toDegrees(Math.atan(y / helper));
        if ((z < 0.0D) && (x < 0.0D)) {
            newYaw = (float)(90.0D + Math.toDegrees(Math.atan(z / x)));
        } else if ((z < 0.0D) && (x > 0.0D)) {
            newYaw = (float)(-90.0D + Math.toDegrees(Math.atan(z / x)));
        }
        return new Float[] { Float.valueOf(newYaw), Float.valueOf(newPitch) };
    }

    public static void blinkToPos(double[] startPos, BlockPos endPos, double slack)
    {
        double curX = startPos[0];
        double curY = startPos[1];
        double curZ = startPos[2];
        double endX = endPos.getX() + 0.5D;
        double endY = endPos.getY() + 1.0D;
        double endZ = endPos.getZ() + 0.5D;
        double distance = Math.abs(curX - endX) + Math.abs(curY - endY) + Math.abs(curZ - endZ);
        int count = 0;
        while (distance > slack)
        {
            distance = Math.abs(curX - endX) + Math.abs(curY - endY) + Math.abs(curZ - endZ);
            if (count > 120) {
                break;
            }
            boolean next = false;
            double diffX = curX - endX;
            double diffY = curY - endY;
            double diffZ = curZ - endZ;
            double offset = (count & 0x1) == 0 ? 0.4D : 0.1D;
            if (diffX < 0.0D) {
                if (Math.abs(diffX) > offset) {
                    curX += offset;
                } else {
                    curX += Math.abs(diffX);
                }
            }
            if (diffX > 0.0D) {
                if (Math.abs(diffX) > offset) {
                    curX -= offset;
                } else {
                    curX -= Math.abs(diffX);
                }
            }
            if (diffY < 0.0D) {
                if (Math.abs(diffY) > 0.25D) {
                    curY += 0.25D;
                } else {
                    curY += Math.abs(diffY);
                }
            }
            if (diffY > 0.0D) {
                if (Math.abs(diffY) > 0.25D) {
                    curY -= 0.25D;
                } else {
                    curY -= Math.abs(diffY);
                }
            }
            if (diffZ < 0.0D) {
                if (Math.abs(diffZ) > offset) {
                    curZ += offset;
                } else {
                    curZ += Math.abs(diffZ);
                }
            }
            if (diffZ > 0.0D) {
                if (Math.abs(diffZ) > offset) {
                    curZ -= offset;
                } else {
                    curZ -= Math.abs(diffZ);
                }
            }
            Minecraft.getMinecraft().getNetHandler().addToSendQueue(new C03PacketPlayer.C04PacketPlayerPosition(curX, curY, curZ, true));
            count++;
        }
    }
}
