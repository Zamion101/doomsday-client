package me.Zamion101.DoomsDayClient.utils;

import me.Zamion101.DoomsDayClient.commands.DCommands;
import net.minecraft.client.entity.EntityPlayerSP;

import java.util.HashMap;
import java.util.Map;

public class CommandManager {

    static Map<String,DCommands> commands = new HashMap<>();

    public static void addCommand(DCommands command){
        if(commandExist(command.getCommand())) return;
        commands.put(command.getCommand(),command);
    }

    public static void executeCommand(String command, String[] args, EntityPlayerSP player){
        if(!commandExist(command)) return;
        DCommands cmd = commands.get(command);
        cmd.getModule().execute(args,player);
    }


    public static boolean commandExist(String command){
        return commands.containsKey(command);
    }
}
