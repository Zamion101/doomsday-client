package me.Komut.DoomsDayClient;

import me.Komut.DoomsDayClient.komutlar.NBTCrash;
import me.Komut.DoomsDayClient.komutlar.VanillaCreative;
import me.Zamion101.DoomsDayClient.commands.DCommands;
import me.Zamion101.DoomsDayClient.utils.CommandManager;


public class KMain {

    public static KMain theKMain;

    public KMain() {
        theKMain = this;
        initCommands();
    }

    public void initCommands() {
        CommandManager.addCommand(new DCommands("nbtcrash", new NBTCrash()));
        CommandManager.addCommand(new DCommands("vanillacreative", new VanillaCreative()));
    }
}