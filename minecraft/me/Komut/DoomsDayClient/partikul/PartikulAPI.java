package me.Komut.DoomsDayClient.partikul;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import net.minecraft.client.gui.Gui;

public class PartikulAPI {

    private final List<Particle> particles;
    private int width, height, count;
    final int partikulBoyut = 1;

    public PartikulAPI(final int width, final int height) {
        this.width = width;
        this.height = height;
        this.count = 95;
        this.particles = new ArrayList<Particle>();
        for (int count = 0; count <= this.count; ++count) {
            this.particles.add(new Particle(new Random().nextInt(width), new Random().nextInt(height)));
        }
    }

    public void partikulCiz() { this.particles.forEach(particle -> particle.partikulCiz()); }

    public class Particle {

        private int xPos, yPos;

        public Particle(final int xPos, final int yPos) {
            this.xPos = xPos;
            this.yPos = yPos;

        }

        public void partikulCiz() {
            this.xPos += new Random().nextInt(2);
            this.yPos += new Random().nextInt(2);

            if(this.xPos > PartikulAPI.this.width) {
                this.xPos = -partikulBoyut;
            }
            if(this.yPos > PartikulAPI.this.height) {
                this.yPos = -partikulBoyut;
            }

            Gui.drawRect(this.xPos, this.yPos, this.xPos + partikulBoyut , this.yPos + partikulBoyut, Color.white.getRGB());
        }
    }
}
