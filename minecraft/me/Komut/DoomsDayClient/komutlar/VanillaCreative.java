package me.Komut.DoomsDayClient.komutlar;

import me.Zamion101.DoomsDayClient.commands.DCommands;
import me.Zamion101.DoomsDayClient.commands.ICommand;
import me.Zamion101.DoomsDayClient.utils.CommandManager;
import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.*;
import net.minecraft.network.play.client.C10PacketCreativeInventoryAction;
import net.minecraft.world.WorldSettings;

public class VanillaCreative implements ICommand{

    @Override
    public void execute(String[] args, EntityPlayerSP player) {
        try {
            Minecraft.getMinecraft().playerController.setGameType((Minecraft.getMinecraft().playerController.getCurrentGameType() == WorldSettings.GameType.CREATIVE) ? WorldSettings.GameType.SURVIVAL : WorldSettings.GameType.CREATIVE);
            System.out.println("<VanillaCreative.java> [VCREATIVE] SUCCESS! Gamemode has been updated");
        } catch (Exception e) {
            System.out.println("<VanillaCreative.java> [VCREATIVE] ERROR! Usage: .vanillacreative");
        }

    }
}


